provider "aws" {
  assume_role {
    role_arn = "arn:aws:iam::938285887320:role/ItoTerraformExecutionRole"
  }
}

module "network" {
  source = "git::https://gitlab.com/eponas/epona.git//modules/aws/patterns/network?ref=v0.2.1"

  name = "ito-delivery-network"

  tags = {
    Owner       = "ito"
    Environment = "delivery"
    ManagedBy   = "epona"
  }

  cidr_block = "10.245.0.0/16"

  availability_zones = ["ap-northeast-1a", "ap-northeast-1c"]

  public_subnets             = ["10.245.1.0/24", "10.245.2.0/24"]
  private_subnets            = ["10.245.3.0/24", "10.245.4.0/24"]
  nat_gateway_deploy_subnets = ["10.245.1.0/24", "10.245.2.0/24"]

  flow_log_enabled = false
}
