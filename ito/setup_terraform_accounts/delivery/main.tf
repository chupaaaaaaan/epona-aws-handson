provider "aws" {
}

module "delivery" {
  source = "git::https://gitlab.com/eponas/epona.git//modules/aws/new_environment/delivery?ref=v0.2.1"

  prefix        = "ito"
  name          = "ito"
  force_destroy = false
  runtime_accounts = {
    "Staging" = "922032444791"
  }
}

module "cross_account_assume_role" {
  source = "git::https://gitlab.com/eponas/epona.git//modules/aws/new_environment/cross_account_assume_policy?ref=v0.2.1"

  execution_role_map = {
    "Staging" = "arn:aws:iam::922032444791:role/ItoTerraformExecutionRole"
  }
  prefix            = "ito"
  terraformer_users = module.delivery.terraformer_users
}
